package az.ibar.stock.messages;

import az.ibar.stock.messages.model.NotificationProducer;
import az.ibar.stock.model.OrderDto;
import az.ibar.stock.model.OrderType;
import az.ibar.stock.repository.OrderRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static az.ibar.stock.config.Serializer.convertToJson;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("Stock MessageListener tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CreateOrderMessageListener.class})
class NotificationProducerListenerTest {

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private ObjectMapper objectMapper;

    @Autowired
    private CreateOrderMessageListener createOrderMessageListener;


    @Test
    void handle() throws Exception {
        var orderDto = OrderDto.builder()
                .targetPrice(BigDecimal.valueOf(865))
                .stockId(1L)
                .orderType(OrderType.BUY)
                .accountNumber("3801")
                .quantity(1)
                .orderId(1L)
                .build();

        NotificationProducer<OrderDto> mailProducer = new NotificationProducer<>(orderDto);

        when(objectMapper.readValue(anyString(), any(TypeReference.class)))
                .thenReturn(mailProducer);

        createOrderMessageListener.handle(convertToJson(mailProducer));

        verify(orderRepository, times(1)).saveOrder(orderDto);
    }

}