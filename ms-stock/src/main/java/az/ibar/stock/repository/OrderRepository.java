package az.ibar.stock.repository;

import az.ibar.stock.model.OrderDto;

import java.util.List;

public interface OrderRepository {

    void saveOrder(OrderDto order);

    void deleteOrder(OrderDto order);

    List<OrderDto> getOrders();

}
