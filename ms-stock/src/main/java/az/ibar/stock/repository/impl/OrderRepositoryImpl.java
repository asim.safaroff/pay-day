package az.ibar.stock.repository.impl;

import az.ibar.stock.model.OrderDto;
import az.ibar.stock.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Repository;

import java.util.List;

import static az.ibar.stock.util.DefaultValueUtil.ORDERS_BUCKET_NAME;

@Repository
@RequiredArgsConstructor
@Slf4j
public class OrderRepositoryImpl implements OrderRepository {

    private final RedissonClient redissonClient;

    @Override
    public void saveOrder(OrderDto order) {
        redissonClient.getList(ORDERS_BUCKET_NAME).add(order);
    }

    public void deleteOrder(OrderDto order) {
        redissonClient.getList(ORDERS_BUCKET_NAME).remove(order);
    }


    @Override
    public List<OrderDto> getOrders() {
        return redissonClient.getList(ORDERS_BUCKET_NAME);
    }

}
