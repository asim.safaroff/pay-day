package az.ibar.stock.messages.binder;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.SubscribableChannel;

public interface OrderCreateStateIn {

    String INPUT = "orderCreateStateInput";

    @Input(OrderCreateStateIn.INPUT)
    SubscribableChannel input();

}
