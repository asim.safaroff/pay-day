package az.ibar.stock.service;

import az.ibar.stock.model.Stock;

import java.util.List;

public interface StockService {

    void updateStockPrice();

    List<Stock> getStocks();

}
