package az.ibar.stock.service.impl;

import az.ibar.stock.model.Stock;
import az.ibar.stock.repository.StockRepository;
import az.ibar.stock.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static az.ibar.stock.util.DefaultValueUtil.BITCOIN_ID;
import static az.ibar.stock.util.DefaultValueUtil.BITCOIN_NAME;
import static az.ibar.stock.util.DefaultValueUtil.DEFAULT_PRICE_MAX;
import static az.ibar.stock.util.DefaultValueUtil.DEFAULT_PRICE_MIN;
import static az.ibar.stock.util.DefaultValueUtil.TESLA_ID;
import static az.ibar.stock.util.DefaultValueUtil.TESLA_NAME;

@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService {


    private final StockRepository stockRepository;

    /**
     * Getting random stocks price and saving to cache as a real-time stock prices
     */
    @Override
    public void updateStockPrice() {
        var tesla = getRealTimeStockPrice(TESLA_ID, TESLA_NAME);
        var bitcoin = getRealTimeStockPrice(BITCOIN_ID, BITCOIN_NAME);

        stockRepository.saveStock(List.of(tesla, bitcoin));
    }

    @Override
    public List<Stock> getStocks() {
        return stockRepository.getStocks();
    }


    private Stock getRealTimeStockPrice(long stockId, String stockName) {
        return Stock.builder()
                .id(stockId)
                .name(stockName)
                .price(generateRandomBigDecimalFromRange())
                .build();
    }

    private static BigDecimal generateRandomBigDecimalFromRange() {
        var randomBigDecimal = DEFAULT_PRICE_MIN
                .add(BigDecimal.valueOf(Math.random())
                        .multiply(DEFAULT_PRICE_MAX.subtract(DEFAULT_PRICE_MIN)));
        return randomBigDecimal.setScale(2, RoundingMode.HALF_EVEN);
    }
}
