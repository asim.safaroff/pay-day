package az.ibar.notification.service;

import az.ibar.notification.client.model.UserDto;

public interface UserService {

    UserDto findById(Long userId);
}
