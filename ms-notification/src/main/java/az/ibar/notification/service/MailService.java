package az.ibar.notification.service;

public interface MailService {

    void sendMail(String mailTo, String subject, String message);

}
