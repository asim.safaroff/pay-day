package az.ibar.order.service;

import az.ibar.order.client.account.model.AccountStock;
import az.ibar.order.model.dto.OrderDto;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {

    void holdAccountAmount(OrderDto orderDto);

    BigDecimal getAccountBalance(String accountNumber);

    List<AccountStock> getAccountStocks(String accountNumber);

}
