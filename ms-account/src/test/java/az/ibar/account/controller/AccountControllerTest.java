package az.ibar.account.controller;

import az.ibar.account.error.AccountNotFoundException;
import az.ibar.account.model.dto.AccountBalanceResponseDto;
import az.ibar.account.model.dto.AccountDepositDto;
import az.ibar.account.model.dto.AccountHoldDto;
import az.ibar.account.model.dto.AccountRequestDto;
import az.ibar.account.model.dto.AccountResponseDto;
import az.ibar.account.model.dto.AccountStockDto;
import az.ibar.account.model.dto.OrderType;
import az.ibar.account.service.AccountService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.List;

import static az.ibar.account.config.Serializer.convertToJson;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Account controller unit tests")
@ExtendWith(SpringExtension.class)
@WebMvcTest(AccountController.class)
class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountService accountService;

    private final String ACCOUNT_NUMBER = "38810944000000000000";
    private final BigDecimal ACCOUNT_BALANCE = BigDecimal.valueOf(5000);
    private final Long USER_ID = 1L;


    @Test
    @DisplayName("Given AccountRequestDto when POST create-account then should return account")
    void save() throws Exception {
        var accountResponseDto = AccountResponseDto.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .balance(ACCOUNT_BALANCE)
                .userId(USER_ID)
                .build();

        var accountRequestDto = AccountRequestDto.builder()
                .userId(USER_ID)
                .build();

        when(accountService.save(any())).thenReturn(accountResponseDto);

        mvc.perform(post("/accounts")
                .contentType(APPLICATION_JSON)
                .content(convertToJson(accountRequestDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.data.accountNumber").value(equalTo("38810944000000000000")));
    }

    @Test
    @DisplayName("Given AccountHoldDto when PUT hol-amount then hold-amount")
    void holdAmount() throws Exception {
        var accountHoldDto = AccountHoldDto.builder()
                .holdAmount(BigDecimal.valueOf(2000))
                .build();

        mvc.perform(put("/accounts/{accountNumber}/hold-amount", ACCOUNT_NUMBER)
                .contentType(APPLICATION_JSON)
                .content(convertToJson(accountHoldDto)))
                .andExpect(status().isOk());

        verify(accountService, times(1)).holdAmount(any(), any());
    }


    @Test
    @DisplayName("Given accountNumber when GET account balance then should return AccountBalanceResponseDto")
    void getAccountCashBalance() throws Exception {
        var accountBalanceResponseDto = AccountBalanceResponseDto.of(ACCOUNT_BALANCE);

        when(accountService.getAccountCashBalance(any())).thenReturn(accountBalanceResponseDto);

        mvc.perform(get("/accounts/{accountNumber}/cash-balance", ACCOUNT_NUMBER))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.data.balance").value(equalTo(accountBalanceResponseDto.getBalance().intValue())));
    }


    @Test
    @DisplayName("Given non existence accountNumber when GET account-balance then throw AccountNotFoundException")
    void getAccountCashBalanceErrorCase() throws Exception {
        when(accountService.getAccountCashBalance(any())).thenThrow(AccountNotFoundException.class);

        mvc.perform(get("/accounts/{accountNumber}/cash-balance", ACCOUNT_NUMBER))
                .andExpect(status().isNotFound());
    }


    @Test
    @DisplayName("Given accountNumber when GET account-stock then should return List of AccountStockDto")
    void getAccountStocks() throws Exception {
        var accountStockDto = AccountStockDto.builder()
                .stockId(USER_ID)
                .accountNumber("38810944000000000000")
                .orderPrice(BigDecimal.valueOf(5000))
                .quantity(2)
                .orderType(OrderType.BUY)
                .build();

        when(accountService.getAccountStocks(any())).thenReturn(List.of(accountStockDto));

        mvc.perform(get("/accounts/{accountNumber}/stocks", ACCOUNT_NUMBER))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].accountNumber").value(equalTo(accountStockDto.getAccountNumber())));
    }


    @Test
    @DisplayName("Given accountNumber when GET account-total-balance then should return AccountBalanceResponseDto")
    void getAccountTotalBalance() throws Exception {

        BigDecimal totalBalance = ACCOUNT_BALANCE;

        when(accountService.getAccountTotalBalance(any())).thenReturn(AccountBalanceResponseDto.of(totalBalance));

        mvc.perform(get("/accounts/{accountNumber}/total-balance", ACCOUNT_NUMBER))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.data.balance").value(equalTo(totalBalance.intValue())));
    }


    @Test
    @DisplayName("Given non existence accountNumber when GET total-balance then throw AccountNotFoundException")
    void getAccountTotalBalanceErrorCase() throws Exception {
        when(accountService.getAccountTotalBalance(any())).thenThrow(AccountNotFoundException.class);

        mvc.perform(get("/accounts/{accountNumber}/total-balance", ACCOUNT_NUMBER))
                .andExpect(status().isNotFound());
    }


    @Test
    @DisplayName("Given accountNumber when PUT deposit then should return HttpStatus")
    void depositAccount() throws Exception {
        var accountDepositDto = AccountDepositDto.builder()
                .depositAmount(BigDecimal.valueOf(2000))
                .build();

        mvc.perform(put("/accounts/{accountNumber}/deposit", ACCOUNT_NUMBER)
                .contentType(APPLICATION_JSON)
                .content(convertToJson(accountDepositDto)))
                .andExpect(status().isOk());

        verify(accountService, times(1)).depositAccount(any(), any());
    }
}