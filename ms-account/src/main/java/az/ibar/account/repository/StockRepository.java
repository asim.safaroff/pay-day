package az.ibar.account.repository;

import az.ibar.account.model.entity.Stock;

import java.util.List;

public interface StockRepository {

    List<Stock> getStocks();

}
