package az.ibar.account.service;

import az.ibar.account.model.entity.Stock;

import java.util.List;

public interface StockService {

    List<Stock> getStocks();

}
