package az.ibar.account.service;

import az.ibar.account.messages.model.AccountStockConsumer;
import az.ibar.account.model.dto.AccountBalanceResponseDto;
import az.ibar.account.model.dto.AccountDepositDto;
import az.ibar.account.model.dto.AccountHoldDto;
import az.ibar.account.model.dto.AccountRequestDto;
import az.ibar.account.model.dto.AccountResponseDto;
import az.ibar.account.model.dto.AccountStockDto;

import java.util.List;

public interface AccountService {

    AccountResponseDto save(AccountRequestDto requestDto);

    void update(AccountStockConsumer dto);

    void holdAmount(String accountNumber, AccountHoldDto dto);

    AccountBalanceResponseDto getAccountCashBalance(String accountNumber);

    List<AccountStockDto> getAccountStocks(String accountNumber);

    AccountBalanceResponseDto getAccountTotalBalance(String accountNumber);

    void depositAccount(String accountNumber, AccountDepositDto dto);
}
