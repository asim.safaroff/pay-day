package az.ibar.account.service.impl;

import az.ibar.account.model.entity.Stock;
import az.ibar.account.repository.StockRepository;
import az.ibar.account.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService {

    private final StockRepository stockRepository;

    @Override
    public List<Stock> getStocks() {
        return stockRepository.getStocks();
    }
}
