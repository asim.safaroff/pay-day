package az.ibar.user.controller;

import az.ibar.user.model.dto.UserDto;
import az.ibar.user.model.dto.UserResponseDto;
import az.ibar.user.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static az.ibar.user.config.Serializer.convertToJson;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("User controller unit tests")
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService userService;

    private final static Long userId = 1L;
    private static UserResponseDto userResponseDto;


    @BeforeAll
    public static void setup() {
        userResponseDto = UserResponseDto.builder()
                .name("Test")
                .id(userId)
                .email("test@test.com")
                .build();
    }

    @Test
    @DisplayName("Given UserDto when POST UserDto then should return userId")
    void save() throws Exception {
        var userDto = UserDto.builder()
                .name("Test")
                .email("test@test.com")
                .build();

        when(userService.save(any())).thenReturn(userResponseDto);

        mvc.perform(post("/users")
                .contentType(APPLICATION_JSON)
                .content(convertToJson(userDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.data.email").value(equalTo("test@test.com")));
    }


    @Test
    @DisplayName("Given userId when GET user then should return UserDto")
    void findUser() throws Exception {
        when(userService.findById(any())).thenReturn(userResponseDto);

        mvc.perform(get("/users").param("userId", userId.toString()))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(jsonPath("$.data.email").value(equalTo("test@test.com")));
    }


}