package az.ibar.user.service.impl;

import az.ibar.user.error.DataMismatchException;
import az.ibar.user.error.UserNotFoundException;
import az.ibar.user.mapper.UserMapper;
import az.ibar.user.messages.MessageSender;
import az.ibar.user.model.dto.UserDto;
import az.ibar.user.model.dto.UserResponseDto;
import az.ibar.user.model.entity.User;
import az.ibar.user.repository.UserRepository;
import az.ibar.user.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("User service tests")
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UserServiceImpl.class})
class UserServiceImplTest {

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserMapper userMapper;

    @MockBean
    private MessageSender<String> messageSender;

    @Autowired
    private UserService userService;

    private final static Long USER_ID = 1L;
    private final static String EMAIL = "test@test.com";
    private final static String NAME = "Test";


    @Test
    @DisplayName("Given UserDto when saveUser then should return userId")
    void saveUser() {
        var user = User.builder()
                .id(USER_ID)
                .name(NAME)
                .email(EMAIL)
                .build();

        var userDto = UserDto.builder()
                .name(NAME)
                .email(EMAIL)
                .build();

        var userResponseDto = UserResponseDto.builder()
                .id(USER_ID)
                .build();

        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());

        when(userMapper.toUserEntity(any())).thenReturn(user);

        when(userRepository.save(any())).thenReturn(user);

        when(userMapper.toUserResponseDto(any())).thenReturn(userResponseDto);


        UserResponseDto result = userService.save(userDto);


        verify(userRepository, times(1)).save(user);

        assertEquals(USER_ID, result.getId());
    }


    @Test
    @DisplayName("Given existence email when saveUser then should throw DataMismatchException")
    void givenBadArgument_whenSaveUser_thenThrowDataMismatchException() {
        var user = User.builder()
                .id(USER_ID)
                .name(NAME)
                .email(EMAIL)
                .build();

        var userDto = UserDto.builder()
                .name(NAME)
                .email(EMAIL)
                .build();

        when(userRepository.findByEmail(any())).thenReturn(Optional.of(user));

        assertThatExceptionOfType(DataMismatchException.class)
                .isThrownBy(() -> userService.save(userDto))
                .withMessage("Email address already exist");

        verify(userRepository, times(1)).findByEmail(any());
    }


    @Test
    @DisplayName("Given userId when findUser then should return UserDto")
    void findUserById() {
        var user = User.builder()
                .id(USER_ID)
                .name(NAME)
                .email(EMAIL)
                .build();

        var userResponseDto = UserResponseDto.builder()
                .id(USER_ID)
                .build();

        when(userRepository.findById(any(Long.class))).thenReturn(Optional.of(user));

        when(userMapper.toUserResponseDto(any(User.class))).thenReturn(userResponseDto);

        UserResponseDto actual = userService.findById(USER_ID);

        verify(userRepository, times(1)).findById(USER_ID);

        assertEquals(USER_ID, actual.getId());
    }


    @Test
    @DisplayName("Given incorrect userId when findUser then throw  UserNotFoundException")
    void findUserByIncorrectId() {
        when(userRepository.findById(any(Long.class)))
                .thenReturn(Optional.empty());

        assertThatExceptionOfType(UserNotFoundException.class)
                .isThrownBy(() -> userService.findById(USER_ID))
                .withMessage("User not found");

        verify(userRepository, times(1)).findById(USER_ID);
    }
}