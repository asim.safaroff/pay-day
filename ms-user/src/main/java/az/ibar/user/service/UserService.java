package az.ibar.user.service;

import az.ibar.user.model.dto.UserDto;
import az.ibar.user.model.dto.UserResponseDto;

public interface UserService {

    UserResponseDto save(UserDto dto);

    UserResponseDto findById(Long id);

}
