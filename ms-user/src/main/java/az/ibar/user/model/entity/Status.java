package az.ibar.user.model.entity;

public enum Status {

    ACTIVE, INACTIVE
}
